section .text
 
; Принимает код возврата и завершает текущий процесс
exit: 
    mov rax, 60
    syscall

; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
    mov rax, rdi
    .count: cmp byte[rdi], 0
    je .end
    inc rdi
    jmp .count
    .end: sub rdi, rax
    mov rax, rdi
    ret

print_string:
    mov rsi, rdi
    push rsi
    call string_length
    pop rsi
    mov rdx, rax
    mov rax, 1
    mov rdi, 1
    syscall
    ret

print_char:
    mov rax, rdi
    push rax
    mov [rsp], al
    mov rsi, rsp
    mov rdx, 1
    mov rdi, 1
    mov rax, 1
    syscall
    pop r8
    ret

; Переводит строку (выводит символ с кодом 0xA)
print_newline:
    push r8
    mov byte [rsp], 0xA
    mov rsi, rsp
    mov rdi, 1
    mov rdx, 1
    mov rax, 1
    syscall
    pop r8
    ret

string_equals:
    sub rsp, 16
    mov [rsp], rdi
    mov [rsp+8], rsi
    push rsi
    call string_length
    pop rsi
    mov r8, rax
    mov rdi, rsi
    push r8
    call string_length
    pop r8
    mov r9, rax
    cmp r8, r9
    jne .diff   ;there is difference between length of two strings
    .compare:
        mov rax, [rsp+8]    ;send char to reg (just a crutch)
        mov rdx, [rsp]
        mov rax, [rax]
        mov rdx, [rdx]
        cmp al, dl      ;compare them
        jne .diff
        inc byte [rsp]
        inc byte [rsp+8]
        dec r8           ;substract length
        jl .same        ;is length equals to 0
        jmp .compare
    .same:                  ;if not ends it with 1
        mov rax, 1
        jmp .end
    .diff:
        xor rax, rax
    .end:
        add rsp, 16;;
        ret

; Выводит беззнаковое 8-байтовое число в десятичном формате 
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint:
    mov rcx, 10
    mov rax, rdi
    xor r8, r8
    dec rsp
    mov byte [rsp], 0;make a null first element
    inc r8
    .divider_loop:
        xor rdx, rdx
        div rcx     
        add dl, 48  ;make a print ready char
        dec rsp
        mov [rsp], dl
        inc r8      ;count chars in number-string
        test rax, rax
        jnz .divider_loop
    .end:
        mov rdi, rsp
        push r8
        call print_string
        pop r8
        add rsp, r8
        ret

; Выводит знаковое 8-байтовое число в десятичном формате 
print_int:
    test rdi, rdi
    jge .unbound_integer
    push rdi
    mov rdi, '-'
    call print_char
    pop rdi
    neg rdi
    .unbound_integer:
        call print_uint
    ret

; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
    push r8
    mov rax,0
    mov rdi,0
    mov rsi,rsp
    mov rdx,1
    syscall
    test rax,rax
    jz .eof_char
    mov al,[rsp]
    .eof_char:
        pop r8
    ret 

; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор

read_word:
    mov r8,rdi  ;adress of the buffer
    mov r9,rsi  ;size of the buffer
    dec r9      ;cut buffer for \0 byte
    mov r10,r8
    add r10,r9 ;last byte memory pointer
    .skip_spaces:    
        push r8
        push r9
        push r10
        push rdi
        call read_char
        pop rdi
        pop r10
        pop r9
        pop r8

        cmp al,0
        je .word_end
        cmp al,0x20
        je .skip_spaces
        cmp al,0x9
        je .skip_spaces
        cmp al,0xA
        je .skip_spaces
        mov byte [r8],al
        inc r8
        cmp r8,r10
        jge  .buffer_is_smaller
    .parse_new_char:
        push r8
        push r9
        push r10
        push rdi
        call read_char
        pop rdi
        pop r10
        pop r9
        pop r8

        cmp al,0
        je .word_end
        cmp al,0x20
        je .word_end
        cmp al,0x9
        je .word_end
        cmp al,0xA
        je .word_end
        mov byte [r8],al
        inc r8
        cmp r8,r10
        jge  .buffer_is_smaller
        jmp .parse_new_char
    .word_end:
        mov byte[r8],0
        mov rax,rdi
        sub r8,rdi
        mov rdx,r8
        ret
    .buffer_is_smaller:
        mov rax, 0
        mov rdx, 0
        ret




; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
    mov r10, 10
    mov r9, rdi ;pointer
    xor r11,r11 ;result
    .parse_char:
        xor rax,rax
        mov al,[r9]
        cmp al,'0'
        jl .not_charnum ;?>48
        cmp al,'9'
        jg .not_charnum ;?<57
        cmp al,0
        je .not_charnum
        mov r8,rax
        mov rax,r11
        mul r10
        mov r11,rax
        mov rax,r8
    .add_num:
        sub al,48
        add r11,rax
        inc r9
        jmp .parse_char
    .not_charnum:       ;error/end: not in[0,9] ~ eof
        sub r9,rdi
        mov rdx,r9
        mov rax,r11
        ret



; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был) 
; rdx = 0 если число прочитать не удалось
parse_int:
    xor rax,rax
    xor r8,r8   ;bool: signed int?
    mov al,[rdi]
    cmp al,'-'
    jne .uint
    inc rdi ;move pointer to uint's start
    inc r8
    .uint:
        push r8
        push r8
        call parse_uint
        test rdx,rdx
        pop r8
        pop r8
        je .end
        test r8,r8
        je .end
        neg rax
        inc rdx
    .end:
        ret

; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy:
    push rdi ;p_string
    push rsi ;p_buff
    push rdx ;buffer lenght
    push rdx
    call string_length
    pop rdx
    pop rdx
    pop rsi
    pop rdi
    inc rax ;add byte for \0
    cmp rax,rdx
    jg .buffer_is_smaller
    mov r8,rdi
    add r8,rax
    mov r9,rdi
    .move_char:
        mov al,[rdi]
        mov [rsi],al
        inc rsi
        inc rdi
        cmp rdi,r8
        jl .move_char
        mov rax,r9
        jmp .end
    .buffer_is_smaller:
        xor rax,rax
    .end:
        ret
